import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IAnnotation } from 'src/common';
import { environment } from 'src/environments/environment';

import { AnnotationService } from '../../../services/annotation/annotation.service';
import { BabylonService } from '../../../services/babylon/babylon.service';
import { ProcessingService } from '../../../services/processing/processing.service';
import { UserdataService } from '../../../services/userdata/userdata.service';
import { BackendService } from 'src/app/services/backend/backend.service';
// tslint:disable-next-line:max-line-length
import { DialogAnnotationEditorComponent } from '../../dialogs/dialog-annotation-editor/dialog-annotation-editor.component';

@Component({
  selector: 'app-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.scss'],
})
export class AnnotationComponent implements OnInit {
  @Input() entityFileName: string | undefined;
  @Input() annotation: IAnnotation | undefined;

  // internal
  public isEditMode = false;
  public showAnnotation = false;
  public collapsed = false;
  public selectedAnnotation: string | undefined;
  public isValid = true;
  public isConnectedToWikibase = false;
  public repoUrl = environment.repo_url;

  // external
  public visibility = false;
  public isAnnotatingAllowed = false;
  public isAnnotationOwner = false;
  public adminContact = "";


  constructor(
    public annotationService: AnnotationService,
    public babylon: BabylonService,
    public dialog: MatDialog,
    private userdataService: UserdataService,
    public processing: ProcessingService,
    private backend: BackendService,
  ) {}

  ngOnInit() {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    this.showAnnotation = true;
    this.collapsed = false;
    this.isAnnotatingAllowed = this.processing.annotationAllowance;
    this.isAnnotationOwner = this.userdataService.isAnnotationOwner(this.annotation);
    this.isConnectedToWikibase = this.isWikibaseIdSet();


    this.annotationService.isSelectedAnnotation.subscribe(selectedAnno => {
      if (!this.annotation) {
        console.error('AnnotationComponent without annotation', this);
        throw new Error('AnnotationComponent without annotation');
      }
      this.visibility = selectedAnno === this.annotation._id;
      this.selectedAnnotation = selectedAnno;
    });

    this.processing.setAnnotationAllowance.subscribe((allowed: boolean) => {
      this.isAnnotatingAllowed = allowed;
    });

    this.annotationService.isEditModeAnnotation.subscribe(this.handleEditModeChange.bind(this));
  }

  get previewImage() {
    const preview = this.annotation?.body.content.relatedPerspective.preview;
    if (!preview) return undefined;
    return preview.startsWith('data:image') ? preview : environment.server_url + preview;
  }

  get userOwnsCompilation() {
    return this.userdataService.userOwnsCompilation;
  }

  public closeAnnotation(): void {
    this.annotationService.setSelectedAnnotation('');
  }

  public async getAdminContact() {
    if (this.adminContact === "") {
      this.adminContact = await this.backend.getAdminContact();
    }
    return this.adminContact;
  }

  public openAnnotation(): void {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    this.annotationService.setSelectedAnnotation(this.annotation._id.toString());
  }

  public toggleEditViewMode(): void {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    // if we are in edit mode, check if the annotation is valid

    this.isValid = this.isValidAnnotation(this.annotation);
    this.isConnectedToWikibase = this.isWikibaseIdSet();

    if (this.isEditMode && !this.isValid) {
      return;
    }
    this.annotationService.setEditModeAnnotation(
      this.isEditMode ? '' : this.annotation._id.toString(),
    );
  }
  
  public handleEditModeChange(selectedEditAnno : any) {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    const isEditAnno = selectedEditAnno === this.annotation._id;
    if (!isEditAnno && this.isEditMode) {
      this.isEditMode = false;
      this.annotationService.updateAnnotation(this.annotation);
    }
    if (isEditAnno && !this.isEditMode) {
      this.isEditMode = true;

      // this.annotationService.setSelectedAnnotation(this.annotation._id);
    }
  }

  public shareAnnotation() {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    this.annotationService.shareAnnotation(this.annotation);
  }

  public deleteAnnotation(): void {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    this.annotationService.deleteAnnotation(this.annotation);
  }

  public editFullscreen(): void {
    if (!this.annotation) {
      console.error('AnnotationComponent without annotation', this);
      throw new Error('AnnotationComponent without annotation');
    }
    const dialogRef = this.dialog.open(DialogAnnotationEditorComponent, {
      width: '75%',
      data: {
        isAnnotatingAllowed: this.isAnnotatingAllowed,
        isAnnotationOwner: this.isAnnotationOwner,
        title: this.annotation.body.content.title,
        content: this.annotation.body.content.description,
        descriptionLicenses: this.annotation.body.content.descriptionLicenses,
        descriptionAuthors: this.annotation.body.content.descriptionAuthors,
        relatedMedia: this.annotation.body.content.relatedMedia,
        relatedMediaUrls: this.annotation.body.content.relatedMediaUrls,
        relatedConcepts: this.annotation.body.content.relatedEntities,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && this.annotation) {
        if (result.remove) {
          this.deleteAnnotation();
          return;
        }
        this.annotation.body.content.title = result.title;
        this.annotation.body.content.description = result.content;
        this.annotation.body.content.descriptionLicenses = result.descriptionLicenses;
        this.annotation.body.content.descriptionAuthors = result.descriptionAuthors;
        this.annotation.body.content.relatedMedia = result.relatedMedia;
        this.annotation.body.content.relatedMediaUrls = result.relatedMediaUrls;
        this.annotation.body.content.relatedEntities = result.relatedConcepts;
        this.toggleEditViewMode();
      }
    });
  }

  public isValidAnnotation(annotation: IAnnotation | undefined): boolean {
    if (!annotation) {
      console.error('AnnotationComponent without annotation', this);
      return false;
    }
    if (!annotation.body.content.title) {
      this.getAdminContact();
      return false;
    }
    return true;
  }

  public isWikibaseIdSet() : boolean {
    return this.annotation?.wikibase_id !== undefined;
  }
}
