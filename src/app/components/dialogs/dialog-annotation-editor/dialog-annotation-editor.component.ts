import { BehaviorSubject, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

import { environment } from '../../../../environments/environment';
import { IEntity, IWikibaseItem } from 'src/common';

import { AnnotationService } from '../../../services/annotation/annotation.service';

export interface IDialogData {
  isAnnotatingAllowed: boolean;
  isAnnotationOwner: boolean;
  title: string;
  content: string;
  url: string;
  relatedMedia: IWikibaseItem[];
  relatedMediaUrls: string[];
  relatedConcepts: IWikibaseItem[];
  descriptionLicenses: IWikibaseItem[];
  descriptionAuthors: IWikibaseItem[];
}

interface IExternalImage {
  description: string;
  url: string;
  mediaType: string;
}

@Component({
  selector: 'app-dialog-annotation-editor',
  templateUrl: './dialog-annotation-editor.component.html',
  styleUrls: ['./dialog-annotation-editor.component.scss'],
})
export class DialogAnnotationEditorComponent {
  @ViewChild('annotationContent')
  private annotationContent: ElementRef<HTMLTextAreaElement> | undefined;

  public editMode = true;
  public labelMode = 'remove_red_eye';
  public labelModeText = 'View';

  public relatedMediaUrl = '';
  public relatedConcept = '';
  private serverUrl = environment.server_url;

  // searchable items
  public searchConcepts = new FormControl('');
  public searchMedia = new FormControl('');
  public searchAgents = new FormControl('');
  public searchLicenses = new FormControl('');
  public availableConcepts = new BehaviorSubject<IWikibaseItem[]>([]);
  public availableMedia = new BehaviorSubject<IWikibaseItem[]>([]);
  public availableAgents = new BehaviorSubject<IWikibaseItem[]>([]);
  public availableLicenses = new BehaviorSubject<IWikibaseItem[]>([]);
  public filteredConcepts$: Observable<IWikibaseItem[]>;
  public filteredMedia$: Observable<IWikibaseItem[]>;
  public filteredAgents$: Observable<IWikibaseItem[]>;
  public filteredLicenses$: Observable<IWikibaseItem[]>;
  public selectedConcept$ = new BehaviorSubject<IWikibaseItem | undefined>(undefined);
  public selectedMedium$ = new BehaviorSubject<IWikibaseItem | undefined>(undefined);
  public selectedAgent$ = new BehaviorSubject<IWikibaseItem | undefined>(undefined);
  public selectedLicense$ = new BehaviorSubject<IWikibaseItem | undefined>(undefined);

  constructor(
    public dialogRef: MatDialogRef<DialogAnnotationEditorComponent>,
    public annotations: AnnotationService,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData,
  ) {
    this.annotations.concepts$.subscribe(concepts => {
      this.availableConcepts.next(concepts);
    });

    this.annotations.media$.subscribe(media => {
      this.availableMedia.next(media);
    });

    this.annotations.agents$.subscribe(agents => {
      this.availableAgents.next(agents);
    });

    this.annotations.licenses$.subscribe(licenses => {
      this.availableLicenses.next(licenses);
    });

    this.filteredConcepts$ = this.searchConcepts.valueChanges.pipe(
      startWith(''),
      map(value => {
        if ((typeof value === 'string' || value instanceof String) && value.length) {
          return this.availableConcepts.value.filter(c =>
            (c.label['en'] + c.description).toLowerCase().includes(value.toLowerCase()),
          );
        }
        return [];
      }),
    );
    this.filteredMedia$ = this.searchMedia.valueChanges.pipe(
      startWith(''),
      map(value => {
        if ((typeof value === 'string' || value instanceof String) && value.length) {
          return this.availableMedia.value.filter(m =>
            (m.label['en'] + m.description).toLowerCase().includes(value.toLowerCase()),
          );
        }
        return [];
      }),
    );
    this.filteredAgents$ = this.searchAgents.valueChanges.pipe(
      startWith(''),
      map(value => {
        if ((typeof value === 'string' || value instanceof String) && value.length) {
          return this.availableAgents.value.filter(a =>
            (a.label['en'] + a.description).toLowerCase().includes(value.toLowerCase()),
          );
        }
        return [];
      }),
    );
    this.filteredLicenses$ = this.searchLicenses.valueChanges.pipe(
      startWith(''),
      map(value => {
        if ((typeof value === 'string' || value instanceof String) && value.length) {
          return this.availableLicenses.value.filter(l =>
            (l.label['en'] + l.description).toLowerCase().includes(value.toLowerCase()),
          );
        }
        return [];
      }),
    );
  }

  public addEntitySwitch(entity: IEntity | IExternalImage) {
    switch (entity.mediaType) {
      case 'externalImage':
        this.addExternalImage(entity as IExternalImage);
        break;

      case 'video':
      case 'audio':
      case 'image':
      case 'text':
      case 'entity':
      case 'model':
        this.addEntity(entity as IEntity);
        break;
      default:
        console.error(`Unknown media type ${entity.mediaType}`);
    }
  }

  public addRelatedMedia() {
    if (this.relatedMediaUrl.length) {
      if (this.data.relatedMediaUrls === undefined) this.data.relatedMediaUrls = [];
      this.data.relatedMediaUrls.push(this.relatedMediaUrl);
      this.relatedMediaUrl = '';
    }

    const mediaItem = this.selectedMedium$.value;
    if (mediaItem != null) {
      if (this.data.relatedMedia === undefined) this.data.relatedMedia = [];
      if (this.data.relatedMedia.find(media => media.id === mediaItem.id) === undefined) {
        // mediaItem.media = this.getPreviewThumbnail(mediaItem);
        this.data.relatedMedia.push({ ...mediaItem });
      }
      this.searchMedia.setValue('', { emitEvent: false });
      this.selectedMedium$.next(undefined);
    }
  }

  public removeRelatedMedia(searchTerm: string) {
    const index = this.data.relatedMedia.findIndex(x => x.id === searchTerm);
    if (index > -1) {
      this.data.relatedMedia.splice(index, 1);
    }
  }

  public removeRelatedMediaUrls(searchTerm: string) {
    const index = this.data.relatedMediaUrls.findIndex(x => x === searchTerm);
    if (index > -1) {
      this.data.relatedMediaUrls.splice(index, 1);
    }
  }

  public addRelatedConcept() {
    const conceptItem = this.selectedConcept$.value;
    if (conceptItem != null) {
      if (this.data.relatedConcepts === undefined) this.data.relatedConcepts = [];
      if (this.data.relatedConcepts.find(concept => concept.id === conceptItem.id) === undefined) {
        this.data.relatedConcepts.push({ ...conceptItem });
      }
      this.searchConcepts.setValue('', { emitEvent: false });
      this.selectedConcept$.next(undefined);
    }
  }

  public addAgentsAndLicenses() {
    const licenseItem = this.selectedLicense$.value;
    const agentItem = this.selectedAgent$.value;
    if (licenseItem != null) {
      if (this.data.descriptionLicenses === undefined) this.data.descriptionLicenses = [];
      if (this.data.descriptionLicenses.find(license => license.id === licenseItem.id) === undefined) {
        this.data.descriptionLicenses.push({ ...licenseItem });
      }
      this.searchLicenses.setValue('', { emitEvent: false });
      this.selectedLicense$.next(undefined);
    }
    if (agentItem != null) {
      if (this.data.descriptionAuthors === undefined) this.data.descriptionAuthors = [];
      if (this.data.descriptionAuthors.find(agent => agent.id === agentItem.id) === undefined) {
        this.data.descriptionAuthors.push({ ...agentItem });
      }
      this.searchAgents.setValue('', { emitEvent: false });
      this.selectedAgent$.next(undefined);
    }
  }

  public removeRelatedConcept(searchTerm: string) {
    const index = this.data.relatedConcepts.findIndex(x => x.id === searchTerm);
    if (index > -1) {
      this.data.relatedConcepts.splice(index, 1);
    }
  }

  public removeDescriptionAuthor(searchTerm: string) {
    const index = this.data.descriptionAuthors.findIndex(x => x.id === searchTerm);
    if (index > -1) {
      this.data.descriptionAuthors.splice(index, 1);
    }
  }

  public removeDescriptionLicense(searchTerm: string) {
    const index = this.data.descriptionLicenses.findIndex(x => x.id === searchTerm);
    if (index > -1) {
      this.data.descriptionLicenses.splice(index, 1);
    }
  }

  private getCaretPosition() {
    if (!this.annotationContent) return { start: 0, value: '' };
    this.annotationContent.nativeElement.focus();

    return {
      start: this.annotationContent.nativeElement.selectionStart,
      value: this.annotationContent.nativeElement.value,
    };
  }

  private createMarkdown(mdElement: any) {
    const caret = this.getCaretPosition();
    const start = caret.start;
    const value = caret.value;

    return `${value.substring(0, start)}${mdElement}${value.substring(start, value.length)}`;
  }

  private addExternalImage(image: IExternalImage) {
    this.data.content = this.createMarkdown(`![alt ${image.description}](${image.url})`);
  }

  private addEntity(entity: IEntity) {
    const target = `${environment.repo_url}/entity/${entity._id}`;
    let url = '';

    let markdown = '';
    switch (entity.mediaType) {
      case 'video':
        if (!entity.dataSource.isExternal) url += `${this.serverUrl}`;
        markdown += `
        <video class="video" controls poster="">
          <source src="${url}/${entity.processed.medium}" type="video/mp4">
        </video>`;
        break;
      case 'audio':
        if (!entity.dataSource.isExternal) url += `${this.serverUrl}`;
        markdown += `
        <audio controls>
          <source src="${url}${entity.processed.medium}" type="audio/mpeg">
        </audio>`;
        break;
      case 'image':
      case 'text':
      case 'model':
      case 'entity':
      default:
        markdown += `
        <a href="${target}" target="_blank">
          <img src="${environment.server_url + entity.settings.preview}" alt="${entity.name}">
        </a>`;
    }

    this.data.content = this.createMarkdown(markdown);
  }

  public selectConcept(event: MatAutocompleteSelectedEvent) {
    const concept = this.availableConcepts.value.find(c => c.id === event.option.value.id);
    if (!concept) {
      return console.warn(`Could not find concept with id ${event.option.value.id}`);
    }
    this.selectedConcept$.next(concept);
  }

  public selectMedium(event: MatAutocompleteSelectedEvent) {
    const medium = this.availableMedia.value.find(m => m.id === event.option.value.id);
    if (!medium) {
      return console.warn(`Could not find medium with id ${event.option.value.id}`);
    }
    this.selectedMedium$.next(medium);
  }

  public selectAgent(event: MatAutocompleteSelectedEvent) {
    const agent = this.availableAgents.value.find(a => a.id === event.option.value.id);
    if (!agent) {
      return console.warn(`Could not find agent with id ${event.option.value.id}`);
    }
    this.selectedAgent$.next(agent);
  }

  public selectLicense(event: MatAutocompleteSelectedEvent) {
    const license = this.availableLicenses.value.find(l => l.id === event.option.value.id);
    if (!license) {
      return console.warn(`Could not find license with id ${event.option.value.id}`);
    }
    this.selectedLicense$.next(license);
  }

  public toggleEditViewMode() {
    if (this.editMode) {
      this.editMode = false;
      this.labelMode = 'edit';
      this.labelModeText = 'Edit';
    } else {
      this.editMode = true;
      this.labelMode = 'remove_red_eye';
      this.labelModeText = 'View';
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }


  public displayWikibaseItemLabel(item: IWikibaseItem): string {
    if (item === undefined || item.label === undefined) return '';
    return item.label['en'] || '';
  }

  // public displayWikibaseItemLabel(item: IWikibaseItem): string {
  //   if (item === undefined || item.label === undefined) return '';
  //   return item.label['en'] || '';
  // }

  // public getPreviewThumbnail(item: IWikibaseItem): string {
  //   const explUrl = item.id.split('/');
  //   const id = explUrl[explUrl.length - 1];
  //   const wikiEnv = explUrl[2];

  //   if (item === undefined || item.id === undefined) return '';
  //   const thumbnailUrl = `https://${wikiEnv}/?title=Special:Redirect/file/Preview${id}.png`;

  //   return thumbnailUrl;
  // }

}
