import {
  MeshBuilder,
  Scene,
  Space,
  StandardMaterial,
  Tags,
  Vector3,
} from 'babylonjs';

const calcRadius = (scene: Scene) => {
  // Try to get size from root mesh, only works for glTF models
  // const rootMesh = scene.getMeshByName('__root__');
  // if (rootMesh) {
  //   const rootSize = rootMesh?._boundingInfo?.boundingBox.maximumWorld;
  //   if (rootSize) {
  //     const avgRootAxisLength = rootSize.asArray().sort()[1];
  //     return avgRootAxisLength * 0.025;
  //   }
  // }

  // Calculate marker size from mesh bounding box
  // let debug_min = Infinity;
  // let debug_max = 0.0;
  // let radius = 0.0;
  // let count = 0;
  // for (const mesh of scene.meshes) {
  //   if (!mesh._boundingInfo) continue;
  //   const boundingRadius = mesh.getBoundingInfo().boundingSphere.radiusWorld;

  //   if (boundingRadius < debug_min) {
  //       debug_min = boundingRadius;
  //   }
  //   if (boundingRadius > debug_max) {
  //       debug_max = boundingRadius;
  //   }

  //   // online mean computation
  //   // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
  //   // in short:
  //   // delta = newValue - mean
  //   // mean += (mean - delta) / count
  //   if (isFinite(boundingRadius) && boundingRadius > 0.000001) {
  //       count += 1;

  //       radius += (boundingRadius - radius) / count;
  //   }
  // }
  // console.log(`estimated marker radius as ${radius} in [${debug_min},${debug_max}]`);
  console.debug(scene.meshes);
  return 0.1;
};

export const createMarker = (
    scene: Scene,
    ranking: string,
    id: string,
    transparent: boolean,
    color?: string,
    position?: Vector3,
    normal?: Vector3,
) => {
  const radius = calcRadius(scene);

  // Create dynamic texture and write the text
  
  console.log(`createMarker(${ranking}, ${id}, ${transparent}, ${color}, ${position}, ${normal})`);
  const mat = new StandardMaterial(`${id}_material${transparent ? '_transparent' : ''}`, scene);
    
  
  const markerName = `${id}_marker${transparent ? '_transparent' : ''}`;
  const marker = MeshBuilder.CreateDisc(markerName, { radius }, scene);
  Tags.AddTagsTo(marker, 'marker');
  Tags.AddTagsTo(marker, transparent ? 'transparent_marker' : 'solid_marker');
  Tags.AddTagsTo(marker, id);
  Tags.AddTagsTo(marker, markerName);
  if (position && normal) {
    marker.position = position;
    marker.translate(normal, 0.5, Space.WORLD);
  }
  marker.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
  marker.material = mat;
  //make marker transparent
  marker.renderingGroupId = transparent ? 3 : 2;

  return marker;
};
